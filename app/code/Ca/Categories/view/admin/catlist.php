<hr>
<div class="categories-list">
<?php foreach ($data['categories'] as $category): ?>
    <div class="row">
        <div class="category-name">
            <?= $category->getName(); ?>
        </div>
        <div class="category-slug">
            <?= $category->getSlug(); ?>
        </div>
        <div class="category-parrent_id">
            <?= $category->getParentId(); ?>
        </div>
        <div class="edit">
            <a href="/categories/edit/<?= $category->getId() ?>">Edit</a>
        </div>
        <form method="post" action="/categories/remove">
            <input type="hidden" name="id" value="<?= $category->getId() ?>">
            <input type="submit" value="X">
        </form>
    </div>
<?php endforeach;?>
</div>
<hr>