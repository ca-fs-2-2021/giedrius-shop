<?php
?>
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
          <link rel="stylesheet"  href="http://127.0.0.1:8001/css/styles.css">     
    <title>Codeacadey Shop</title>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark  bg-dark">
        <div class="container-fluid px-5">
            <a class="navbar-brand text-warning" href="#">Shop</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" href="/">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="/products" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" >
                            Products
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="/products">Edit products</a></li>
                            <li><a class="dropdown-item" href="/products/create">Create product</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="/categories" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" >
                            Categories
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="/categories">Edit categories</a></li>
                            <li><a class="dropdown-item" href="/categories/create">Create category</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<main class="container justify-content-center">


